# publish-to-npm-test

Demo for publishig a package to the npm registry through a CI job using 
Jenkins and the [pipeline npm plugin](https://plugins.jenkins.io/pipeline-npm/), which reads a npm config file created via the `Managed files` funcionality of Jenkins (Manage Jenkins-> Managed Files -> Add a new config -> Npm config file.)

There, the content of the file will be:
```
//registry.npmjs.org/:_authToken=NPM_TOKEN
```

Where `NPM_TOKEN` is your authentication token created in NPM.

In this demo we use the registry is the official npm one, but it can be any other.
